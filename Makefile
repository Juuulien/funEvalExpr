##
## EPITECH PROJECT, 2019
## fun_funEvalExpr
## File description:
## Makefile
##

NAME	=	funEvalExpr

all: $(NAME)

$(NAME):
	stack path --local-install-root
	stack build $@
	cp -f .stack-work/install/x86_64-linux-tinfo6/lts-14.11/8.6.5/bin/funEvalExpr-exe $(NAME)

.PHONY: $(NAME)

tests_run:
	stack test

clean:
	stack clean

fclean: clean
	@rm -f $(NAME)

re: fclean all